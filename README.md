<!--
    SPDX-License-Identifier: CC0-1.0
    SPDX-FileCopyrightText: 2023 Harald Sitter <sitter@kde.org>
-->

Kind of like a podman toolbox but using systemd. The scripts assemble an overlay and that overlay can then be used as systemd-sysext.
The primary difference from toolbox is that this is an overlay of your actual / that makes it more easily used with systemd-sysext and
that in turn makes it easier to deal with development of non-application software (such as KDE Plasma) on read-only /usr systems.

```
git clone https://invent.kde.org/sitter/ueberzug $HOME/ueberzug
cd $HOME/ueberzug
./create # create overlay
./enter # enter overlay
./extend # create systemd-sysext from overlay
```

`extend` creates a systemd-sysext called 40-ueberzug. If you then have a kdesrc-build in ~/kf6 you can stack 50-kf6 on top of the ueberzug.
Building software is all done inside the ueberzug namespace meaning you don't have to have a writable /usr (or polluted it with deps).
It's super advisable to keep a bootstrap script so the ueberzug is easily replicated by installing all relevant development packages etc.

You can of course also boot the ueberzug via systemd-nspawn at which point you can run any old systemd unit inside. Notably you can
run sshd and then attach to that with your editor (if the editor supports it) to do remote programming.

# Use Case

The complete use case looks like this:

- /usr is not writable
- $HOME/kf6 contains a kdesrc-build
- systemd-sysext of ueberzug extends necessary libraries and headers into /usr (enabling use without LD_LIBRARY_PATH trickery etc)
- systemd-sysext of kf6 extends the actual KDE software build into /usr (enabling proper support of polkit and dbus services)

When developing using ueberzug I'd enter the namespace using `./enter`. I get dropped into a regular terminal session of my user
with $HOME fully intact, plus whatever gets set up by .bashrc. I can then install additional dependencies into the namespace
(and thus the overlay) to expose them to the system via the systemd-sysext. Once all dependencies are installed I can build
software as per usual and either install it into /usr (exposing it via the ueberzeug extension) or $HOME/kf6/ (exposing it via
the kf6 extension). The latter in particular is simply how we use kdesrc-build, you could technically opt to always install
everything into /usr using this approach.

When `systemd-sysext merge` is run on the host, the development environment is merged into the host's /usr and I can use
SDDM to log into a Plasma session of the development build. Conversely when `systemd-sysext unmerge` is run the pristine
host /usr is restored back and I can log into a working stable session instead.

# Caveat

The software must be capable of working without populated /etc and /var as those do not get merged by systemd-sysext. Usually that's
simply a matter of making sure that assets live in /usr or can be otherwise loaded at runtime (e.g. from qrc)
